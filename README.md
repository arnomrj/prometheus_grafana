# Prometheus + Grafana

Este repositório é um tutorial para Prometheus e Grafana.

Para rodar, execute os comandos abaixo:
```
docker-compose up --build
```

Url dos serviços funcionando:
- Grafana: http://localhost:3000
- Prometheus: http://localhost:9090
- Fake App: http://localhost:5000 

### Grafana
- usuário: admin
- senha: admin
